package com.exigotechro.acr122u;

import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;


public class AwsMqttPublisher implements MqttCallback
{
    MqttClient client;
    String topic;
    String msgBody;
    String broker;
    String clientId;
    int qos;
    private String _replyTopic;

    private static String   AWS_MQTT_TOPIC     = "both_directions";
    private static String   AWS_MQTT_BROKER    = "tcp://192.168.11.25:1883";
    private static String   AWS_MQTT_CLIENT_ID = "AwsMqttPublisher-01";
    private static Integer  AWS_MQTT_QOS       = 2;

    public AwsMqttPublisher()
    {
        this(
                AWS_MQTT_TOPIC,
                "{\"status\":\"OK\"}",
                AWS_MQTT_BROKER,
                AWS_MQTT_CLIENT_ID,
                AWS_MQTT_QOS
        );
    }

    public AwsMqttPublisher(String msgTopic, String msgBody)
    {
        this(
                msgTopic,
                msgBody,
                AWS_MQTT_BROKER,
                AWS_MQTT_CLIENT_ID,
                AWS_MQTT_QOS
        );
    }

    public AwsMqttPublisher( String topic, String msgBody, String broker, String clientId, int qos )
    {
        this.setTopic(topic);
        this.setMsgBody(msgBody);
        this.setBroker(broker);
        this.setClientId(clientId);
        this.setQos(qos);

        this.setReplyTopic(null);
    }

    public static void main(String[] args) {
        String device_id = null;
        try {
            device_id = "00000000b83e345e"/*RPi3Device.GetDeviceId()*/;

            String msgTopic = String.format("%s/%s/%s/%s", "rpi3", device_id, "nfcid", "007");
            String msgBody = "{\"device_id\":\"" + device_id + "\"}";

            System.out.println("=================\nmsg topic: " + msgTopic + "\n=================");
            System.out.println("=================\nmsg body: " + msgBody + "\n=================");

            AwsMqttPublisher myAwsMqttPub = new AwsMqttPublisher(msgTopic, msgBody);

            // test publishing a message
            myAwsMqttPub.publishMqttMessage();


            if(!true)
            {
                // test subscribing to a topic
                String replyTopic = String.format("%s/%s/%s/%s", "aws", device_id, "nfcid", "007");
                myAwsMqttPub.subscribeToMqttTopic(replyTopic);

                int cntr = 1;
                while(cntr <= 10){
                    Thread.sleep(1000);
                    cntr += 1;
                }

                myAwsMqttPub.unsubscribeFromMqttTopic();
                myAwsMqttPub.disconnectMqttClient();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void publishMqttMessage(String msgTopic, String msgBody)
    {
        this.setTopic(msgTopic);
        this.setMsgBody(msgBody);
        this.publishMqttMessage();
    }


    private void bringUpMqttClient()
    {
        MemoryPersistence persistence = new MemoryPersistence();

        try {
            this.client = new MqttClient(this.broker, this.clientId, persistence);

            MqttConnectOptions connOpts = new MqttConnectOptions();
            connOpts.setCleanSession(true);
            connOpts.setConnectionTimeout(10);
            connOpts.setKeepAliveInterval(10); //how often to send PINGREQ messages
            connOpts.setMqttVersion(MqttConnectOptions.MQTT_VERSION_3_1_1);

            this.client.connect(connOpts);
        }
        catch(MqttException me)
        {
            this.dumpExceptionMsg(me);
        }

    }

    public void subscribeToMqttTopic(String sub_topic)
    {
        this.bringUpMqttClient();
        this.setReplyTopic(sub_topic);

        try
        {
            this.client.setCallback(this);
            this.client.subscribe(this.getReplyTopic());
        }
        catch(MqttException me){ this.dumpExceptionMsg(me); }
    }


    public void unsubscribeFromMqttTopic()
    {
        try {
            this.client.unsubscribe(this.getReplyTopic());
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    public void disconnectMqttClient()
    {
        try {
            this.client.disconnect();
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }


    public void publishMqttMessage()
    {
        this.bringUpMqttClient();

        try
        {
            this.client.publish(this.topic, this.msgBody.getBytes(), this.qos, false);
            this.client.disconnect();
        }
        catch(MqttException me) { this.dumpExceptionMsg(me); }
    }

    @Override
    public void connectionLost(Throwable cause) {

    }

    @Override
    public void messageArrived(String topic, MqttMessage message)
            throws Exception
    {
        System.out.println("=> RCVD Msg: \n\t" + message.toString());
    }

    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {

    }


    public void setMsgBody(String _msgBody) {
        this.msgBody = _msgBody;
    }

    public void setTopic(String _topic) {
        this.topic = _topic;
    }

    public void setBroker(String _broker) {
        this.broker = _broker;
    }

    public void setClientId(String _clientId) {
        this.clientId = _clientId;
    }

    public void setQos(int _qos) {
        this.qos = _qos;
    }


    public void setReplyTopic(String _replyTopic) {
        this._replyTopic = _replyTopic;
    }

    public String getReplyTopic() {
        return _replyTopic;
    }



    private void dumpExceptionMsg(MqttException me)
    {
        System.out.println("reason "+me.getReasonCode());
        System.out.println("msg "+me.getMessage());
        System.out.println("loc "+me.getLocalizedMessage());
        System.out.println("cause "+me.getCause());
        System.out.println("excep "+me);
        me.printStackTrace();
    }



}


   /*
   * Publish message to topic 'both_directions'
   * mosquitto_pub -h localhost -p 1883 -q 1 -d -t both_directions  -i clientid1 -m "{\"key\": \"helloFromLocalGateway\"}"
   *
   * Subscribe to messages on topic 'both_directions'
   * mosquitto_sub -v -t both_directions
   *
   * */

