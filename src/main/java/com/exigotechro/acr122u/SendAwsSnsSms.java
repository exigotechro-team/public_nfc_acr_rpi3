package com.exigotechro.acr122u;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.AmazonSNSClientBuilder;
import com.amazonaws.services.sns.model.CreateTopicRequest;
import com.amazonaws.services.sns.model.CreateTopicResult;
import com.amazonaws.services.sns.model.SubscribeRequest;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import com.amazonaws.services.sns.model.DeleteTopicRequest;

public class SendAwsSnsSms
{
    private AmazonSNS snsClient;
    private String topicARN;

    private SendAwsSnsSms()
    {
        snsClient = getSnsClient();
    }

    public static void main(String[] args)
    {
        SendAwsSnsSms myAwsSns = new SendAwsSnsSms();

        if(!true){
            String topicName = "my_new_topic";
            String topicARN = myAwsSns.createSnsTopic(topicName);
            myAwsSns.setTopicARN(topicARN);
        }

        if(true){
            String testTopicARN = "arn:aws:sns:us-east-1:306430215556:my_new_topic";
            myAwsSns.setTopicARN(testTopicARN);
        }

        if(!true) {
            myAwsSns.subscribeToTopic();
        }

        if(true) {
            String msgBody = "this is a test5";
            myAwsSns.publishToTopic(msgBody);
        }

        if(!true) {
            myAwsSns.deleteSnsTopic();
        }

    }

    private void setTopicARN(String topicARN) {
        this.topicARN = topicARN;
    }


    private static AmazonSNS getSnsClient()
    {
/*
        BasicAWSCredentials awsCreds = new BasicAWSCredentials("AKIAIKTUGY6Q6TG7W6MQ", "2ZmJHhVRBuRXAhH+BS3D4suiXrU2FBCuvkLtyiwZ");

        return AmazonSNSClientBuilder
                .standard()
                .withRegion(Regions.US_EAST_1)
                .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
                .build();
*/

        return AmazonSNSClientBuilder.defaultClient();

    }

    private void publishToTopic(String msg)
    {
        PublishRequest publishRequest = new PublishRequest(this.topicARN, msg);
        PublishResult publishResult = this.snsClient.publish(publishRequest);
        System.out.println("MessageId - " + publishResult.getMessageId());
    }


    private String createSnsTopic(String topicName)
    {
        //create a new SNS topic
        CreateTopicRequest createTopicRequest = new CreateTopicRequest(topicName);
        CreateTopicResult createTopicResult = this.snsClient.createTopic(createTopicRequest);
        //print TopicArn
        System.out.println(createTopicResult);
        //get request id for CreateTopicRequest from SNS metadata
        System.out.println("CreateTopicRequest - " + this.snsClient.getCachedResponseMetadata(createTopicRequest));

        return createTopicResult.getTopicArn();
    }


    private void subscribeToTopic()
    {
        //subscribe to an SNS topic
        SubscribeRequest subRequest = new SubscribeRequest(this.topicARN, "email", "adrian.arva@gmail.com");
        this.snsClient.subscribe(subRequest);
        //get request id for SubscribeRequest from SNS metadata
        System.out.println("SubscribeRequest - " + this.snsClient.getCachedResponseMetadata(subRequest));
        System.out.println("Check your email and confirm subscription.");
    }


    private void deleteSnsTopic()
    {
        //delete an SNS topic
        DeleteTopicRequest deleteTopicRequest = new DeleteTopicRequest(this.topicARN);
        this.snsClient.deleteTopic(deleteTopicRequest);
        //get request id for DeleteTopicRequest from SNS metadata
        System.out.println("DeleteTopicRequest - " + this.snsClient.getCachedResponseMetadata(deleteTopicRequest));
    }

}
