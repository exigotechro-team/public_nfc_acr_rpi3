package com.exigotechro.acr122u;

import com.amazonaws.services.iot.client.*;
import com.exigotechro.device.RPi3Device;
import com.exigotechro.mqtt.NfcMsg;
import com.exigotechro.mqtt.aws.DeviceCommandTopicListener;
import com.exigotechro.mqtt.aws.Rpi3NfcTagPublishListener;
import com.exigotechro.utils.AwsIotUtil;
import com.exigotechro.utils.CommandArguments;
import org.joda.time.DateTime;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import javax.smartcardio.*;

public class Main {

    private static HashMap<String, USBTerminal> previousReadings = new HashMap<>();

    private static AWSIotMqttClient awsIotClient = null;

    public Main() {
    }

    public static void main(String[] args)
            throws InterruptedException, AWSIotException, AWSIotTimeoutException, IOException
    {
        String device_id = RPi3Device.GetDeviceId();

        CommandArguments arguments = CommandArguments.parse(args);
        awsIotClient = AwsIotUtil.initAwsIotClient(arguments, awsIotClient);

        awsIotClient.connect();

        String device_subscribe_topic = String.format(NfcMsg.Rpi3DeviceTopicPattern, device_id);
        AWSIotTopic deviceCmdTopic = new DeviceCommandTopicListener(device_subscribe_topic, NfcMsg.DeviceTopicQos);
        awsIotClient.subscribe(deviceCmdTopic, 2000, false);

        try {
            Acr122Utils.SetSystemProperty();
            TerminalFactory factory = TerminalFactory.getDefault();
            List<CardTerminal> terminals = factory.terminals().list(CardTerminals.State.CARD_PRESENT);
            System.out.println("Terminals: " + terminals);
            if(terminals.size() <= 0) {
                throw new Exception("No terminals attached or found\n");
            }

            for (CardTerminal terminal : terminals)
            {

                String terminalName = terminal.getName();

                Card card = terminal.connect("*");
                System.out.println("\n\n=================\nTerminal: " + terminalName + "\t(hash: "+terminalName.hashCode()+")");
                System.out.println("Card: " + card);
//                System.out.println("\tCard ATR: " + eu.verdelhan.acr122urw.HexUtils.bytesToHexString( card.getATR().getBytes()));
                CardChannel channel = card.getBasicChannel();

                if (checkIfCardPresent(terminal, channel))
                {
                    String cardId = Acr122Utils.ReadTagId(channel);

                    USBTerminal prevReading = null;

                    if(!previousReadings.containsKey(terminalName)){
                        previousReadings.put(terminal.getName(), new USBTerminal(null, new DateTime()));
                    }

                    prevReading = (USBTerminal) previousReadings.get(terminalName);

                    if(cardId != null && !cardId.isEmpty()){
                        if(!cardId.equals(prevReading.getCardId())){
                            previousReadings.put(terminalName, new USBTerminal(cardId, new DateTime()));
                        }
                    }

                    System.out.println("Card ID: " + cardId );
                    System.out.println("Device ID: " + device_id + "\n=================");

                    String msgTopic = NfcMsg.GetRpi3NfcTagTopic(device_id, cardId);
                    String msgBody = "{\"terminal\":\""+terminalName+"\",\"card_uid\":\""+cardId+"\",\"device_id\":\""+device_id+"\"}";

                    AWSIotMessage message = new Rpi3NfcTagPublishListener(msgTopic, NfcMsg.DeviceTopicQos, msgBody);

                    try {
//                        awsIotClient.publish(msgTopic, msgBody);
                        awsIotClient.publish(message, 2000);
                    } catch (AWSIotException e) {
                        System.out.println(System.currentTimeMillis() + ": publish failed for " + msgBody);
                    }

                    Acr122Utils.BlinkLED(channel);
                }

                card.disconnect(false);
            }

            System.out.println("\n\nDone cycling the terminals!");

//            awsIotClient.unsubscribe(deviceCmdTopic, 2000);
            awsIotClient.disconnect(2000);
            System.out.println("\n\nawsIotClient disconnected!");

        } catch (Exception var8) {
            System.out.println("Ouch: " + var8.toString());
        }

    }

    private static boolean checkIfCardPresent(CardTerminal cardTerminal, CardChannel channel)
            throws Exception
    {
        boolean isCardPresent = false;

        if(!cardTerminal.isCardPresent())
        {
            System.out.println("*** Insert card");

            Acr122Utils.setBuzzerON(channel);

            if(cardTerminal.waitForCardPresent(10000L)) {
                isCardPresent = true;
            }
        } else {
            isCardPresent = true;
        }

        return isCardPresent;
    }


}