package com.exigotechro.acr122u.tasks;

import java.util.Date;
import java.util.TimerTask;

/**
 * Created by adria on 6/8/2017.
 */
public class EchoTask extends TimerTask
{
    @Override
    public void run()
    {
        System.out.println("\t > " + new Date() + " running..!!");
    }
}
