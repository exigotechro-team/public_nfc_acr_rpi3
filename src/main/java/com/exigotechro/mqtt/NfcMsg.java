package com.exigotechro.mqtt;

import com.amazonaws.services.iot.client.AWSIotQos;

public class NfcMsg
{
    public static String DEVICE_ID = "deviceId";
    public static String NFC_CARD_ID = "tagId";
    public static String TERMINAL_SN = "terminal_sn";
    public static String TERMINAL_NAME = "terminal";

    public static String TOPIC_DEVICE_TYPE = "rpi3";
    public static String TOPIC_NFCID = "nfcid";

    public static final String Rpi3DeviceTopicPattern = "device/%s/command/#";
    public static final AWSIotQos DeviceTopicQos = AWSIotQos.QOS0;

    private static final String Rpi3TopicPattern = "rpi3/%s/nfcid/%s";


    public static String BuildMqttMessage(String terminalName, String terminalSerialNmbr, String cardId, String deviceId)
    {
        return String.format(
                "{" +
                        "\"%s\":\"%s\"," +
                        "\"%s\":\"%s\"," +
                        "\"%s\":\"%s\"," +
                        "\"%s\":\"%s\"" +
                        "}",
                NfcMsg.TERMINAL_NAME,   terminalName,
                NfcMsg.TERMINAL_SN,     terminalSerialNmbr,
                NfcMsg.NFC_CARD_ID,     cardId,
                NfcMsg.DEVICE_ID,       deviceId);
    }

    public static String GetRpi3NfcTagTopic(String device_id, String tag_id){
        return String.format(Rpi3TopicPattern, device_id, tag_id);
    }

    public static String BuildMqttTopic(String[] args)
    {
        String msgTopic = "";

        for(String s: args){
            if(msgTopic.length()==0){
                msgTopic = s;
            }else{
                msgTopic = String.format("%s/%s", msgTopic, s);
            }
        }

        return msgTopic;
    }
}
