/*
 * Copyright 2016 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *  http://aws.amazon.com/apache2.0
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

package com.exigotechro.utils;

import com.amazonaws.services.iot.client.AWSIotMqttClient;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.net.URL;
import java.security.*;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.HashMap;
import java.util.Properties;

/**
 * This is a helper class to facilitate reading of the configurations and
 * certificate from the resource files.
 */
public class AwsIotUtil {
    private static final String PropertyFile = "aws-iot.properties";

    private static Properties prop;
    private static boolean propFileExists;

    private static HashMap<String, KeyStorePasswordPair> KeyStoreForCerts = new HashMap<String, KeyStorePasswordPair>();

    public static class KeyStorePasswordPair {
        public KeyStore keyStore;
        public String keyPassword;

        public KeyStorePasswordPair(KeyStore keyStore, String keyPassword) {
            this.keyStore = keyStore;
            this.keyPassword = keyPassword;
        }
    }

    public static String getConfig(String name) {

        if (prop == null) {
            AwsIotUtil.loadPropertiesFile();
        }

        if (prop != null && propFileExists) {
            String value = prop.getProperty(name);
            if (value == null || value.trim().length() == 0) {
                return null;
            } else {
                return value;
            }
        } else {
            return null;
        }
    }

    private static void loadPropertiesFile() {
        prop = new Properties();
        URL resource = AwsIotUtil.class.getResource(PropertyFile);
        if (resource == null) {
            propFileExists = false;
            return;
        }
        try (InputStream stream = resource.openStream()) {
            prop.load(stream);
            propFileExists = true;
        } catch (IOException e) {
            propFileExists = false;
        }
    }

    public static KeyStorePasswordPair getKeyStorePasswordPair(String certificateFile, String privateKeyFile) {
        return getKeyStorePasswordPair(certificateFile, privateKeyFile, null);
    }

    public static KeyStorePasswordPair getKeyStorePasswordPair(String certificateFile, String privateKeyFile,
                                                               String keyAlgorithm) {
        if (certificateFile == null || privateKeyFile == null) {
            System.out.println("Certificate or private key file missing");
            return null;
        }

        String ks_key = AwsIotUtil.getMD5Hash(String.format("%s_%s", certificateFile, privateKeyFile));

        if(AwsIotUtil.KeyStoreForCerts.containsKey(ks_key)){
            return AwsIotUtil.KeyStoreForCerts.get(ks_key);
        }

        Certificate certificate = loadCertificateFromFile(certificateFile);
        PrivateKey privateKey = loadPrivateKeyFromFile(privateKeyFile, keyAlgorithm);

        if (certificate == null || privateKey == null) {
            return null;
        }

        KeyStorePasswordPair keyPair = getKeyStorePasswordPair(certificate, privateKey);
        AwsIotUtil.KeyStoreForCerts.put(ks_key, keyPair);

        return keyPair;
    }

    public static KeyStorePasswordPair getKeyStorePasswordPair(Certificate certificate, PrivateKey privateKey) {
        KeyStore keyStore = null;
        String keyPassword = null;
        try {
            keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
            keyStore.load(null);
            keyStore.setCertificateEntry("alias", certificate);

            // randomly generated key password for the key in the KeyStore
            keyPassword = new BigInteger(128, new SecureRandom()).toString(32);
            keyStore.setKeyEntry("alias", privateKey, keyPassword.toCharArray(), new Certificate[]{certificate});
        } catch (KeyStoreException | NoSuchAlgorithmException | CertificateException | IOException e) {
            System.out.println("Failed to create key store");
            return null;
        }

        return new KeyStorePasswordPair(keyStore, keyPassword);
    }

    private static Certificate loadCertificateFromFile(String filename) {
        Certificate certificate = null;

        InputStream in = AwsIotUtil.class.getResourceAsStream(filename);

        if (in == null) {
            System.out.println("Certificate file not found: " + filename);
            return null;
        }

        try (BufferedInputStream stream = new BufferedInputStream(in)) {
            CertificateFactory certFactory = CertificateFactory.getInstance("X.509");
            certificate = certFactory.generateCertificate(stream);
        } catch (IOException | CertificateException e) {
            System.out.println("Failed to load certificate file " + filename);
        }


        return certificate;
    }

    private static PrivateKey loadPrivateKeyFromFile(String filename, String algorithm) {
        PrivateKey privateKey = null;

        InputStream in = AwsIotUtil.class.getResourceAsStream(filename);

        if (in == null) {
            System.out.println("Private key file not found: " + filename);
            return null;
        }

        try (DataInputStream stream = new DataInputStream(in)) {
            privateKey = PrivateKeyReader.getPrivateKey(stream, algorithm);
        } catch (IOException | GeneralSecurityException e) {
            System.out.println("Failed to load private key from file " + filename);
        }

        return privateKey;
    }


    private static String getMD5Hash(String plaintext) {
        MessageDigest m = null;
        String hash = null;

        try {
            m = MessageDigest.getInstance("MD5");
            m.reset();
            m.update(plaintext.getBytes());
            byte[] digest = m.digest();
            BigInteger bigInt = new BigInteger(1, digest);
            String hashtext = bigInt.toString(16);
            // Now we need to zero pad it if you actually want the full 32 chars.
            while (hashtext.length() < 32) {
                hashtext = String.format("0%s", hashtext);
            }

            hash = hashtext;

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return hash;
    }


    public static AWSIotMqttClient initAwsIotClient(CommandArguments arguments, AWSIotMqttClient awsIotClient)
    {
        String clientEndpoint = arguments.getNotNull("clientEndpoint", AwsIotUtil.getConfig("clientEndpoint"));
        String clientId = arguments.getNotNull("clientId", AwsIotUtil.getConfig("clientId"));

        String certificateFile = arguments.get("certificateFile", AwsIotUtil.getConfig("certificateFile"));
        String privateKeyFile = arguments.get("privateKeyFile", AwsIotUtil.getConfig("privateKeyFile"));

        if (awsIotClient == null && certificateFile != null && privateKeyFile != null) {
            String algorithm = arguments.get("keyAlgorithm", AwsIotUtil.getConfig("keyAlgorithm"));
            AwsIotUtil.KeyStorePasswordPair pair = AwsIotUtil.getKeyStorePasswordPair(certificateFile, privateKeyFile, algorithm);

            awsIotClient = new AWSIotMqttClient(clientEndpoint, clientId, pair.keyStore, pair.keyPassword);
        }

        if (awsIotClient == null) {
            String awsAccessKeyId = arguments.get("awsAccessKeyId", AwsIotUtil.getConfig("awsAccessKeyId"));
            String awsSecretAccessKey = arguments.get("awsSecretAccessKey", AwsIotUtil.getConfig("awsSecretAccessKey"));
            String sessionToken = arguments.get("sessionToken", AwsIotUtil.getConfig("sessionToken"));

            if (awsAccessKeyId != null && awsSecretAccessKey != null) {
                awsIotClient = new AWSIotMqttClient(clientEndpoint, clientId, awsAccessKeyId, awsSecretAccessKey,
                        sessionToken);
            }
        }

        if (awsIotClient == null) {
            throw new IllegalArgumentException("Failed to construct client due to missing certificate or credentials.");
        }

        return awsIotClient;
    }








}
