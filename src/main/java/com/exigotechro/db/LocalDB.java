package com.exigotechro.db;

import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;

public class LocalDB {

    private AmazonDynamoDB client;
    private DynamoDB dynamoDB;

    public LocalDB() {
        this.setupConnection();
    }

    public void setupConnection()
    {
        this.setClient(
                AmazonDynamoDBClientBuilder.standard()
                .withRegion(Regions.US_EAST_1.toString())
                .withEndpointConfiguration(
                        new AwsClientBuilder.EndpointConfiguration("http://localhost:8000", Regions.US_EAST_1.toString()))
                .build()
        );

        dynamoDB = new DynamoDB(this.getClient());
    }

    public DynamoDB getDynamoDB() {
        return dynamoDB;
    }


    public AmazonDynamoDB getClient() {
        return client;
    }

    public void setClient(AmazonDynamoDB client) {
        this.client = client;
    }


}
