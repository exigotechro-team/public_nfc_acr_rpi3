package com.exigotechro.db;

import com.exigotechro.data.model.DeviceItem;
import com.exigotechro.data.model.RfidTagItem;
import com.exigotechro.data.model.UserItem;
import com.exigotechro.data.repository.*;

public class InitDatabase extends BaseNfcItemsRepository
{

    public InitDatabase(NfcDynamoDb nfcDdb) {
        super(nfcDdb);
    }


    public static void main(String[] args) throws Exception
    {
        NfcDynamoDb.setDEFAULT(NfcDynamoDb.AWS);

        InitDatabase initDb = new InitDatabase(null);

        NfcItemsTable nfcTbl = new NfcItemsTable(nfcDDb);
        nfcTbl.createTable(DeviceItem.TABLE_NAME, 5L, 1L, DeviceItem.TABLE_PK_ID, "S");

        if(true)
        {
            DevicesRepository devicesRep = new DevicesRepository(nfcDDb);
            devicesRep.loadDevicesData(DeviceItem.TABLE_NAME);

            UsersRepository usersRep = new UsersRepository(nfcDDb);
            usersRep.loadUsersData(UserItem.TABLE_NAME);

            RfidTagRepository rfidTagRep = new RfidTagRepository(nfcDDb);
            rfidTagRep.loadRfidTagData(RfidTagItem.TABLE_NAME);
        }

    }


}
