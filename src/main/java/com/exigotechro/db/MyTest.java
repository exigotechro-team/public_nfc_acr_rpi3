package com.exigotechro.db;

import com.exigotechro.data.model.CmdPropertyType;
import com.exigotechro.data.model.CmdPropertiesType;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.Arrays;

public class MyTest {

    public MyTest() {
    }

    public static void main(String[] args){

        CmdPropertyType c = new CmdPropertyType("txt", "s3://my_text.txt");
        System.out.println(c.toString());

        CmdPropertiesType cr = new CmdPropertiesType(Arrays.asList(
                new CmdPropertyType("txt", "s3://my_text.txt"),
                new CmdPropertyType("html", "s3://my_rich_text.html"),
                new CmdPropertyType("txt", "s3://my_text.txt"),
                new CmdPropertyType("txt", "s3://my_text.txt")
        ));
        System.out.println(cr.toString());

        ObjectMapper mapper = new ObjectMapper();
        try {
            CmdPropertiesType crType= mapper.readValue(cr.toString()/*jsonDoc*/, CmdPropertiesType.class);

            System.out.println("\n-------\n"
                    + crType.toString()
                    + "\n-------\n" );

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
