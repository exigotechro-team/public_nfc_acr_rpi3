package com.exigotechro.db;

import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;

public class NfcDynamoDb
{
    public static final int LOCAL  = 1;
    public static final int AWS    = 2;
    public static int DEFAULT    = 2; // LOCAL

    private static AmazonDynamoDB client;
    private static DynamoDB dynamoDB;
    private static DynamoDBMapper dynamoDBMapper;


    public NfcDynamoDb(int dbType)
    {
        if(dbType == NfcDynamoDb.AWS)
        {
            client = AmazonDynamoDBClientBuilder.standard().withCredentials(
                    new ProfileCredentialsProvider("dynamodb_cli")).build();
            dynamoDB = new DynamoDB(client);
        }
        else if(dbType == NfcDynamoDb.LOCAL)
        {
            LocalDB localDb = new LocalDB();
            client  = localDb.getClient();
            dynamoDB  = localDb.getDynamoDB();
        }

        dynamoDBMapper = new DynamoDBMapper(getClient());


    }

    public AmazonDynamoDB getClient() {
        return client;
    }

    public DynamoDB getDynamoDB() {
        return dynamoDB;
    }

    public DynamoDBMapper getDynamoDbMapper() {
        return dynamoDBMapper;
    }

    public static void setDEFAULT(int newDefault) {
        NfcDynamoDb.DEFAULT = newDefault;
    }


}
