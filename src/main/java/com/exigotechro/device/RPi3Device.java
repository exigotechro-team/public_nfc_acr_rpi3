package com.exigotechro.device;

import java.io.*;
import java.util.Map;
import java.util.Properties;

public class RPi3Device
{
//    public static String APP_PROPS_FILE_NAME = "/root/projects/rpi_acr122u_01/resources/app_properties.txt";
    public static String APP_PROPS_FILE_NAME = "./resources/app_properties.txt";
    public static String DEVICE_ID_PROP_KEY = "DEVICE_ID";
    public static String DEVICE_TYPE_PROP_KEY = "DEVICE_TYPE";

    private static String Device_ID = null;


/*
    private static String CPU_SERIAL_COMMAND_TO_FILE = "cat /proc/cpuinfo | grep -e ^Serial | cut -d\":\" -f2 | sed 's/^ *\\(.*\\) *$/\\1/' > /tmp/cpu_serial";
    private static String CPU_SERIAL_COMMAND_v1 = "cat /proc/cpuinfo | grep -e ^Serial | cut -d\":\" -f2 | sed 's/^ *\\(.*\\) *$/\\1/'";
    private static String CPU_SERIAL_COMMAND_v2 = "sh /tmp/get_device_id.sh";
*/

    public static void main(String[] args)
            throws Exception
    {
        String dev_id = RPi3Device.GetDeviceId();
        System.out.println(DEVICE_ID_PROP_KEY + " => " + dev_id);
//        System.getProperties().list(System.out);
    }



    public static String  GetDeviceId()
            throws IOException
    {
        if(RPi3Device.Device_ID != null){
            return RPi3Device.Device_ID;
        }

        String device_id = System.getProperty(DEVICE_ID_PROP_KEY);

        if(device_id == null)
        {
            ConfigureDeviceId();

            device_id = System.getProperty(DEVICE_ID_PROP_KEY);
            RPi3Device.Device_ID = device_id;
        }

        return device_id;
    }

    public static void ConfigureDeviceId()
            throws IOException
    {
        File f = new File(APP_PROPS_FILE_NAME);
        if(!f.exists())
        {
            Properties new_app_props = new Properties();
            new_app_props.setProperty(DEVICE_TYPE_PROP_KEY, "RPi3");

            FileOutputStream out = new FileOutputStream(APP_PROPS_FILE_NAME);
            new_app_props.store(out, "---No Comment---");
            out.close();

//            System.out.println("app_props file created!");

        } // end if ! file.exists

        // set up new properties object from file "myProperties.txt"
        FileInputStream propFile = new FileInputStream(APP_PROPS_FILE_NAME);
        Properties app_props = new Properties(System.getProperties());
        app_props.load(propFile);

        String device_id = "";

        if(!app_props.containsKey(DEVICE_ID_PROP_KEY))
        {
            RPi3Device rPi3Device = new RPi3Device();
            device_id = rPi3Device.getDeviceId();
            app_props.setProperty(DEVICE_ID_PROP_KEY, device_id);

            FileOutputStream out = new FileOutputStream(APP_PROPS_FILE_NAME);
            app_props.store(out, "---No Comment---");
            out.close();

//            System.out.println("DEVICE_ID read from cpuinfo");

        }else{
            device_id = app_props.getProperty(DEVICE_ID_PROP_KEY);

//            System.out.println("DEVICE_ID read from app_props file");
        }

        System.setProperty(DEVICE_ID_PROP_KEY, device_id);
    }


    public String getDeviceId()
    {
        String CPU_SERIAL_COMMAND = "cat /proc/cpuinfo";
        return this.executeCommand(CPU_SERIAL_COMMAND);
    }

    private String executeCommand(String command) {

        StringBuilder output = new StringBuilder();

        Process p;
        try {
            p = Runtime.getRuntime().exec(command);
            p.waitFor();
            BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));

            String line = "";
            while ((line = reader.readLine())!= null)
            {
                if(line.startsWith("Serial"))
                {
                    String dev_id = line.split(":")[1].trim();
                    output.append(dev_id);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return output.toString();

    }
}



