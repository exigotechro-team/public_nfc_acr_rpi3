package com.exigotechro.data.mappers;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.exigotechro.data.model.UserItem;
import com.exigotechro.data.repository.BaseNfcItemsRepository;
import com.exigotechro.db.NfcDynamoDb;


public class UsersMapper extends BaseNfcItemsRepository
{
    public UsersMapper(NfcDynamoDb nfcDdb){
        super(nfcDdb); }

    public static void main(String[] args)
    {
        NfcDynamoDb.setDEFAULT(NfcDynamoDb.AWS);

        UsersMapper userMapperDemo = new UsersMapper(null);
        UserItem userItem = userMapperDemo.loadItem();
    }

    public UserItem loadItem()
    {
        UserItem userItem = new UserItem();
        userItem.setItemId("user::002");

        try {
            UserItem result = ddbMapper.load(userItem);
            if (result != null) {
                System.out.println(
                        String.format("The user %s has user_id %s",
                                result.getUserName(),
                                result.getItemId())
                );
                return result;
            } else {
                System.out.println("No matching user was found");
                return null;
            }
        } catch (Exception e) {
            System.err.println("Unable to retrieve data: ");
            System.err.println(e.getMessage());
        }

        return null;
    }

}