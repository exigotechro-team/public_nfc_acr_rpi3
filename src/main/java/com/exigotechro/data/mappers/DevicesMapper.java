package com.exigotechro.data.mappers;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.exigotechro.data.model.DeviceItem;
import com.exigotechro.data.repository.BaseNfcItemsRepository;
import com.exigotechro.db.NfcDynamoDb;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class DevicesMapper extends BaseNfcItemsRepository
{
    public DevicesMapper(NfcDynamoDb nfcDdb){
        super(nfcDdb);}

    public static void main(String[] args) throws Exception
    {
        DevicesMapper deviceMapperDemo = new DevicesMapper(null);

        List<DeviceItem> devices = deviceMapperDemo.getAllDevices();
//        DeviceItem deviceItem = deviceMapperDemo.loadItem();

    }

    public List<DeviceItem> getAllDevices()
    {
        Map<String, String> attributeNames = new HashMap<String, String>();
        attributeNames.put("#color", "color");

        Map<String, AttributeValue> attributeValues = new HashMap<String, AttributeValue>();
        attributeValues.put(":color", new AttributeValue().withS("red"));

        DynamoDBScanExpression dynamoDBScanExpression = new DynamoDBScanExpression()
                .withFilterExpression("#color = :color")
                .withExpressionAttributeNames(attributeNames)
                .withExpressionAttributeValues(attributeValues);

        List<DeviceItem> devices = ddbMapper.scan(DeviceItem.class, dynamoDBScanExpression);

        for (DeviceItem item : devices){
            System.out.println(item.toString());
        }

        return devices;
    }

    public DeviceItem loadItem()
    {
        DeviceItem keySchema = new DeviceItem();
        keySchema.setItemId("device::003");
        keySchema.setOwnerId("user::004");

        try {
            DeviceItem result = ddbMapper.load(keySchema);
            if (result != null) {
                System.out.println(
                        String.format("The Device '%s' has device_id '%s' and is owned by '%s'",
                                result.getLabel(),
                                result.getItemId(),
                                result.getOwnerId())
                );

                return result;
            } else {
                System.out.println("No matching Device was found");
                return null;
            }
        } catch (Exception e) {
            System.err.println("Unable to retrieve data: ");
            System.err.println(e.getMessage());
        }

        return null;
    }

}