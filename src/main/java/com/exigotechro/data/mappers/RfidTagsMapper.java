package com.exigotechro.data.mappers;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.exigotechro.data.model.RfidTagItem;
import com.exigotechro.data.repository.BaseNfcItemsRepository;
import com.exigotechro.db.NfcDynamoDb;


public class RfidTagsMapper extends BaseNfcItemsRepository
{
    public RfidTagsMapper(NfcDynamoDb nfcDdb){
        super(nfcDdb); }

    public static void main(String[] args)
    {
        RfidTagsMapper rfidTagMapperDemo = new RfidTagsMapper(null);
        RfidTagItem rfidTagItem = rfidTagMapperDemo.loadItem();
    }


    public RfidTagItem loadItem()
    {
        RfidTagItem keySchema = new RfidTagItem();
        keySchema.setTagId("tag::001");
        keySchema.setOwnerId("user::001");

        try {
            RfidTagItem result = ddbMapper.load(keySchema);
            if (result != null) {
                System.out.println(
                        String.format("The RfidTag %s has tag_id %s",
                                result.getLabel(),
                                result.getTagId())
                );

                return result;
            } else {
                System.out.println("No matching RfidTag was found");
                return null;
            }
        } catch (Exception e) {
            System.err.println("Unable to retrieve data: ");
            System.err.println(e.getMessage());
        }

        return null;
    }

} // end class