package com.exigotechro.data.repository;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.exigotechro.data.model.DeviceItem;
import com.exigotechro.data.model.UserItem;
import com.exigotechro.db.NfcDynamoDb;

import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.TimeZone;

public class UsersRepository extends BaseNfcItemsRepository
{

    public UsersRepository(NfcDynamoDb nfcDdb) {
        super(nfcDdb);}

    public static void main(String[] args) throws Exception
    {
        NfcDynamoDb.setDEFAULT(NfcDynamoDb.AWS);

        UsersRepository usersRep = new UsersRepository(null);
        usersRep.loadUsersData(UserItem.TABLE_NAME);
    }

    public void loadUsersData(String _tableName)
    {
        boolean tableExists = checkIfTableExists(_tableName);

        if(!tableExists){
            System.out.println(String.format("Table %s does not exist!",_tableName));
            return; }


        try {
            long time1 = (new Date()).getTime() - (7 * 24 * 60 * 60 * 1000); // 7 days ago

            Date date1 = new Date();
            date1.setTime(time1);


            UserItem.DdbDateFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));

            Table table = nfcDDb.getDynamoDB().getTable(_tableName);

            System.out.println("Adding data to " + _tableName);

            Item item = new Item()
                    .withPrimaryKey("id", "user::001")
                    .withString("userName", "costel")
                    .withString("email", "costel@mail.com")
                    .withBoolean("okToEmail", true)
                    .withString("phone", "+13121231234")
                    .withBoolean("okToText", true)
                    .withStringSet("devices", new HashSet<String>(Arrays.asList("t1", "t2", "t3")))
                    .withStringSet("tagsActive", new HashSet<String>(Arrays.asList("t1", "t2", "t3")))
                    .withStringSet("tagsPending", new HashSet<String>(Arrays.asList("t1", "t2", "t3")))
                    .withString("picture", "s3://devices/device_mugshot_001")
                    .withString("dateAdded", DeviceItem.DdbDateFormatter.format(date1));
            table.putItem(item);


            UserItem userItem = new UserItem();
            userItem.setItemId("user::002");
            userItem.setUserName("steve");
            userItem.setEmail("steve@mail.com");
            userItem.setOkToEmail(true);
            userItem.setPhone("+1313211234");
            userItem.setOkToEmail(true);
            userItem.setDevices(new HashSet<String>(Arrays.asList("rpi1_01", "rpi2_02")));
            userItem.setTagsActive(new HashSet<String>(Arrays.asList("tag111", "tag1325")));
            userItem.setTagsPending(new HashSet<String>(Arrays.asList("tag421", "tag135")));
            userItem.setPicture("s3://user/002/steve.jpg");
            userItem.setDateAdded(new Date());

            DynamoDBMapper mapper = new DynamoDBMapper(nfcDDb.getClient());
            mapper.save(userItem);

        }
        catch (Exception e) {
            System.err.println("Failed to create item in " + _tableName);
            System.err.println(e.getMessage());
        }

    }




}
