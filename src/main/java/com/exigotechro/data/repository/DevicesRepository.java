package com.exigotechro.data.repository;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.exigotechro.data.model.DeviceItem;
import com.exigotechro.db.NfcDynamoDb;

import java.util.*;

public class DevicesRepository extends BaseNfcItemsRepository
{
    public DevicesRepository(NfcDynamoDb nfcDdb) {
        super(nfcDdb); }

    public static void main(String[] args) throws Exception
    {
        DevicesRepository devicesRep = new DevicesRepository(null);
        devicesRep.loadDevicesData(DeviceItem.TABLE_NAME);
    }

    public void loadDevicesData(String _tableName)
    {
        boolean tableExists = checkIfTableExists(_tableName);

        if(!tableExists){
            System.out.println(String.format("Table %s does not exist!",_tableName));
            return; }


        try {
            long time1 = (new Date()).getTime() - (7 * 24 * 60 * 60 * 1000); // 7 days ago

            Date date1 = new Date();
            date1.setTime(time1);


            DeviceItem.DdbDateFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));

            Table table = nfcDDb.getDynamoDB().getTable(_tableName);

            System.out.println("Adding data to " + _tableName);

            Item item = new Item().withPrimaryKey("id", "device::003")
                    .withString("deviceStatus", "off")
                    .withString("deviceAccess", "public")
                    .withStringSet("terminals", new HashSet<String>(Arrays.asList("t1", "t2", "t3")))
                    .withStringSet("tagsActive", new HashSet<String>(Arrays.asList("t1", "t2", "t3")))
                    .withStringSet("tagsPending", new HashSet<String>(Arrays.asList("t1", "t2", "t3")))
                    .withString("label", "RPi3_003")
                    .withString("description", "Lets user check-in/out from a property")
                    .withString("color", "#ff00ee")
                    .withString("shape", "circle")
                    .withString("picture", "s3://devices/device_mugshot_001")
                    .withString("ownerId", "user::007")
                    .withString("dateAdded", DeviceItem.DdbDateFormatter.format(date1));
            table.putItem(item);


            DeviceItem deviceItem = new DeviceItem();
            deviceItem.setItemId("device::004");
            deviceItem.setDeviceStatus("on");
            deviceItem.setDeviceAccess("public");
            deviceItem.setTerminals(new HashSet<String>(Arrays.asList("t11", "t25")));
            deviceItem.setTagsActive(new HashSet<String>(Arrays.asList("tag111", "tag1325")));
            deviceItem.setTagsPending(new HashSet<String>(Arrays.asList("tag421", "tag135")));
            deviceItem.setLabel("RPi3_004");
            deviceItem.setDescription("Smart poster at the lobby entrance");
            deviceItem.setColor("red");
            deviceItem.setShape("square");
            deviceItem.setPicture("s3://device/002/red_square.jpg");
            deviceItem.setOwnerId("user::008");
            deviceItem.setDateAdded(new Date());

            DynamoDBMapper mapper = new DynamoDBMapper(nfcDDb.getClient());
            mapper.save(deviceItem);

        }
        catch (Exception e) {
            System.err.println("Failed to create item in " + _tableName);
            System.err.println(e.getMessage());
        }

    }




}
