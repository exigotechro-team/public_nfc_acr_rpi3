package com.exigotechro.data.repository;

import com.exigotechro.data.model.DeviceItem;
import com.exigotechro.db.NfcDynamoDb;

public class NfcItemsTable extends BaseNfcItemsRepository
{

    public NfcItemsTable(NfcDynamoDb nfcDdb) {
        super(nfcDdb);
    }

    public static void main(String[] args) throws Exception
    {
        NfcItemsTable nfcTbl = new NfcItemsTable(null);
        nfcTbl.createTable(DeviceItem.TABLE_NAME, 1L, 1L, DeviceItem.TABLE_PK_ID, "S");

//        nfcTbl.deleteTable(DeviceItem.TABLE_NAME);
//        nfcTbl.listMyTables();
//        nfcTbl.getTableInformation(DeviceItem.TABLE_NAME);
//        nfcTbl.updateTableRwCapacity(DeviceItem.TABLE_NAME, 2L, 2L);

    }
}
