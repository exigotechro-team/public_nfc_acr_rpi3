package com.exigotechro.data.repository;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.exigotechro.data.model.DeviceItem;
import com.exigotechro.data.model.UserItem;
import com.exigotechro.data.model.RfidTagItem;
import com.exigotechro.db.NfcDynamoDb;

import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.TimeZone;

public class RfidTagRepository extends BaseNfcItemsRepository
{
    public RfidTagRepository(NfcDynamoDb nfcDdb) {
        super(nfcDdb);}

    public static void main(String[] args) throws Exception
    {
        RfidTagRepository rfidTagRep = new RfidTagRepository(null);
        rfidTagRep.loadRfidTagData(RfidTagItem.TABLE_NAME);
    }

    public void loadRfidTagData(String _tableName)
    {
        boolean tableExists = checkIfTableExists(_tableName);

        if(!tableExists){
            System.out.println(String.format("Table %s does not exist!",_tableName));
            return; }


        try {
            long time1 = (new Date()).getTime() - (7 * 24 * 60 * 60 * 1000); // 7 days ago

            Date date1 = new Date();
            date1.setTime(time1);


            RfidTagItem.DdbDateFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));

            Table table = nfcDDb.getDynamoDB().getTable(_tableName);

            System.out.println("Adding data to " + _tableName);

            Item item = new Item()
                    .withPrimaryKey("id", "tag::001")
                    .withString("tagStatus", "active")
                    .withString("label", "Return Home")
                    .withString("description", "Use when returning home")
                    .withString("color", "blue")
                    .withString("shape", "triangle")
                    .withString("picture", "s3://tag/001/red_triangle.jpg")
                    .withString("ownerId", "user::001")
                    .withStringSet("devices", new HashSet<String>(Arrays.asList("device::002", "device::001")))
                    .withString("tagType", "Mifare Ultralight")
                    .withString("dateAdded", RfidTagItem.DdbDateFormatter.format(date1));
            table.putItem(item);

            RfidTagItem rfidTagItem = new RfidTagItem();
            rfidTagItem.setItemId("tag::002");
            rfidTagItem.setTagStatus("active");
            rfidTagItem.setLabel("return home");
            rfidTagItem.setDescription("tag to be scanned when returning home");
            rfidTagItem.setColor("red");
            rfidTagItem.setShape("square");
            rfidTagItem.setPicture("s3://tag/002/red_square.jpg");
            rfidTagItem.setOwnerId("user::008");
            rfidTagItem.setDevices(new HashSet<String>(Arrays.asList("device::002", "device::01")));
            rfidTagItem.setTagType("Mifare NTAG213");
            rfidTagItem.setDateAdded(new Date());


            DynamoDBMapper mapper = new DynamoDBMapper(nfcDDb.getClient());
            mapper.save(rfidTagItem);

        }
        catch (Exception e) {
            System.err.println("Failed to create item in " + _tableName);
            System.err.println(e.getMessage());
        }

    }




}
