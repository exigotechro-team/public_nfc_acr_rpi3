package com.exigotechro.data.repository;

import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.exigotechro.data.model.*;
import com.exigotechro.db.NfcDynamoDb;

import java.util.*;

public class CommandRepository extends BaseNfcItemsRepository
{

    public CommandRepository(NfcDynamoDb nfcDdb) {
        super(nfcDdb); }

    public static void main(String[] args) throws Exception
    {
        NfcDynamoDb.setDEFAULT(NfcDynamoDb.AWS);

        CommandRepository cmndsRep = new CommandRepository(null);
        cmndsRep.loadCommandData(CommandItem.TABLE_NAME);

        CommandItem cmdItem = cmndsRep.loadCommandItem();

        List<CmdPropertyType> cp = cmdItem.getPrincipals().getData();
        cmdItem.getPrincipals().addProperty(new CmdPropertyType("pdf", "s3://my_msg.pdf"));
        cmdItem.getResources().addProperty(new CmdPropertyType("pdf", "s3://my_msg.pdf"));

        cmndsRep.updateCmdItem(cmdItem);

        cmdItem = null;

        cmdItem = cmndsRep.loadCommandItem();


    }

    private void updateCmdItem(CommandItem cmdItem)
    {
        try {
            ddbMapper.save(cmdItem);
        } catch (Exception e) {
            System.err.println("Unable to save data: ");
            System.err.println(e.getMessage());
        }
    }

    private CommandItem loadCommandItem()
    {
        CommandItem cmdItem = new CommandItem();

        cmdItem.setItemId("cmd::006");

        try {
            CommandItem result = ddbMapper.load(cmdItem);
            if (result != null) {
                cmdItem = result;
            } else {
                System.out.println("No matching command was found");
            }
        } catch (Exception e) {
            System.err.println("Unable to retrieve data: ");
            System.err.println(e.getMessage());
        }

        return cmdItem;
    }

    public void loadCommandData(String _tableName)
    {
        boolean tableExists = checkIfTableExists(_tableName);

        if(!tableExists){
            System.out.println(String.format("Table %s does not exist!",_tableName));
            return; }


        try {
            long time1 = (new Date()).getTime() - (7 * 24 * 60 * 60 * 1000); // 7 days ago

            Date date1 = new Date();
            date1.setTime(time1);


            CommandItem.DdbDateFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));

            Table table = nfcDDb.getDynamoDB().getTable(_tableName);

            System.out.println("Adding data to " + _tableName);

            CmdPropertiesType cmdProps = new CmdPropertiesType();
            cmdProps.addProperty(new CmdPropertyType("txt", "s3://my_msg.txt"));
            cmdProps.addProperty(new CmdPropertyType("html", "s3://my_msg.html"));

            Item item = new Item()
                    .withPrimaryKey("id", "cmd::005")
                    .withString("deviceId", "device::001")
                    .withStringSet("terminals", new HashSet<String>(Arrays.asList("t1", "t2", "t3")))
                    .withStringSet("tags", new HashSet<String>(Arrays.asList("tag::001", "tag::002")))
                    .withString("commandStatus", "active")
                    .withString("label", "email costel")
                    .withJSON("resources", cmdProps.toString())
                    .withJSON("principals", cmdProps.toString())
                    .withString("dateAdded", DeviceItem.DdbDateFormatter.format(date1));

            table.putItem(item);


            CommandItem commandItem = new CommandItem();
            commandItem.setItemId("cmd::006");
            commandItem.setDeviceId("device::002");
            commandItem.setTerminals(new HashSet<String>(Arrays.asList("t11", "t22", "t33")));
            commandItem.setTags(new HashSet<String>(Arrays.asList("tag::001", "tag::12322")));
            commandItem.setCommandStatus("active");
            commandItem.setLabel("email to parents");
            commandItem.setPrincipals(cmdProps);
            commandItem.setResources(cmdProps);
            commandItem.setDateAdded(new Date());

//            DynamoDBMapper mapper = new DynamoDBMapper(nfcDDb.getClient());
            ddbMapper.save(commandItem);

        }
        catch (Exception e) {
            System.err.println("Failed to create item in " + _tableName);
            System.err.println(e.getMessage());
        }

    }




}
