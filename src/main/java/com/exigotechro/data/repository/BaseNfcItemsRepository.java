package com.exigotechro.data.repository;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.TableCollection;
import com.amazonaws.services.dynamodbv2.model.*;
import com.exigotechro.db.NfcDynamoDb;

import java.util.ArrayList;
import java.util.Iterator;

public class BaseNfcItemsRepository {

    protected static NfcDynamoDb nfcDDb;
    protected DynamoDBMapper ddbMapper;

    public BaseNfcItemsRepository(NfcDynamoDb nfcDdb) {
        nfcDDb = (nfcDdb != null) ? nfcDdb : new NfcDynamoDb(NfcDynamoDb.DEFAULT);
        ddbMapper = nfcDDb.getDynamoDbMapper();

    }


    public boolean checkIfTableExists(String _tableName) {

        boolean tblExists = false;

        TableCollection<ListTablesResult> tables = nfcDDb.getDynamoDB().listTables();
        Iterator<Table> iterator = tables.iterator();

        while (iterator.hasNext()) {
            Table table = iterator.next();

            if(_tableName.equals(table.getTableName())){
                tblExists = true;
            }
        }

        return tblExists;
    }


    public void createTable(String tableName, long readCapacityUnits, long writeCapacityUnits,
                                    String partitionKeyName, String partitionKeyType) {

        createTable(tableName, readCapacityUnits, writeCapacityUnits, partitionKeyName, partitionKeyType, null, null);
    }


    public void createTable(String tableName, long readCapacityUnits, long writeCapacityUnits,
                                    String partitionKeyName, String partitionKeyType, String sortKeyName, String sortKeyType) {

        try {

            boolean tableExists = checkIfTableExists(tableName);

            if(tableExists){
                System.out.println(String.format("Table %s already exists!",tableName));
                return;
            }

            ArrayList<KeySchemaElement> keySchema = new ArrayList<KeySchemaElement>();
            keySchema.add(new KeySchemaElement().withAttributeName(partitionKeyName).withKeyType(KeyType.HASH)); // Partition key

            ArrayList<AttributeDefinition> attributeDefinitions = new ArrayList<AttributeDefinition>();
            attributeDefinitions.add(new AttributeDefinition().withAttributeName(partitionKeyName).withAttributeType(partitionKeyType));

            if (sortKeyName != null) {
                keySchema.add(new KeySchemaElement().withAttributeName(sortKeyName).withKeyType(KeyType.RANGE)); // Sort key
                attributeDefinitions.add(new AttributeDefinition().withAttributeName(sortKeyName).withAttributeType(sortKeyType));
            }

            CreateTableRequest request = new CreateTableRequest()
                    .withTableName(tableName)
                    .withKeySchema(keySchema)
                    .withProvisionedThroughput(
                            new ProvisionedThroughput()
                                    .withReadCapacityUnits(readCapacityUnits)
                                    .withWriteCapacityUnits(writeCapacityUnits)
                    );
            request.setAttributeDefinitions(attributeDefinitions);

            System.out.println("Issuing CreateTable request for " + tableName);
            Table table = nfcDDb.getDynamoDB().createTable(request);
            System.out.println("Waiting for " + tableName + " to be created...this may take a while...");
            table.waitForActive();

            getTableInformation(tableName);

        }
        catch (Exception e) {
            System.err.println("CreateTable request failed for " + tableName);
            System.err.println(e.getMessage());
        }
    }


    public void listMyTables()
    {
        TableCollection<ListTablesResult> tables = nfcDDb.getDynamoDB().listTables();
        Iterator<Table> iterator = tables.iterator();

        System.out.println("-----------------------------------------");
        System.out.println("\nListing table names");

        while (iterator.hasNext()) {
            Table table = iterator.next();
            System.out.println("\t => " + table.getTableName());
        }
    }


    public void getTableInformation(String _tableName)
    {
        boolean tableExists = checkIfTableExists(_tableName);

        if(!tableExists){
            System.out.println(String.format("Table %s does not exist!",_tableName));
            return; }


        System.out.println("-----------------------------------------");
        System.out.println("\nDescribing " + _tableName);

        TableDescription tableDescription = nfcDDb.getDynamoDB().getTable(_tableName).describe();
        System.out.format(
                "Name: %s:\n" + "Status: %s \n" + "Provisioned Throughput (read capacity units/sec): %d \n"
                        + "Provisioned Throughput (write capacity units/sec): %d \n",
                tableDescription.getTableName(), tableDescription.getTableStatus(),
                tableDescription.getProvisionedThroughput().getReadCapacityUnits(),
                tableDescription.getProvisionedThroughput().getWriteCapacityUnits());
    }


    public void updateTableRwCapacity(String _tableName, Long _writeCapacityUnits, Long _readCapacityUnits)
    {
        boolean tableExists = checkIfTableExists(_tableName);

        if(!tableExists){
            System.out.println(String.format("Table %s does not exist!",_tableName));
            return; }


        Table table = nfcDDb.getDynamoDB().getTable(_tableName);

        System.out.println("-----------------------------------------");
        System.out.println("\nModifying provisioned throughput for " + _tableName);

        try {
            table.updateTable(new ProvisionedThroughput()
                    .withReadCapacityUnits(_readCapacityUnits)
                    .withWriteCapacityUnits(_writeCapacityUnits));

            table.waitForActive();
        }
        catch (Exception e) {
            System.err.println("UpdateTable request failed for " + _tableName);
            System.err.println(e.getMessage());
        }
    }

    public void deleteTable(String _tableName)
    {
        boolean tableExists = checkIfTableExists(_tableName);

        if(!tableExists){
            System.out.println(String.format("Table %s does not exist!",_tableName));
            return; }


        Table table = nfcDDb.getDynamoDB().getTable(_tableName);
        try {
            System.out.println("-----------------------------------------");
            System.out.println("\nIssuing DeleteTable request for " + _tableName);
            table.delete();

            System.out.println("Waiting for " + _tableName + " to be deleted...this may take a while...");

            table.waitForDelete();
        }
        catch (Exception e) {
            System.err.println("DeleteTable request failed for " + _tableName);
            System.err.println(e.getMessage());
        }
    }


    public static NfcDynamoDb getNfcDDb() {
        return nfcDDb;
    }


}
