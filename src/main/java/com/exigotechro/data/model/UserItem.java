package com.exigotechro.data.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class UserItem extends BaseItem
{
    private String userName;
    private String email;
    private boolean okToEmail;
    private String phone;
    private boolean okToText;

    private Set<String> devices;
    private Set<String> tagsActive;
    private Set<String> tagsPending;
    private String picture;
    private Date dateAdded;

    public String getUserId() { return getItemId();}
    public void setUserId(String _userId) {this.setItemId(_userId);}

    @DynamoDBAttribute(attributeName="userName")
    public String getUserName() { return userName;}
    public void setUserName(String _userName) {this.userName = _userName;}

    @DynamoDBAttribute(attributeName = "email")
    public String getEmail() { return email;}
    public void setEmail(String email) {this.email = email;}

    @DynamoDBAttribute(attributeName = "okToEmail")
    public boolean getOkToEmail() { return okToEmail; }
    public void setOkToEmail(boolean ok_to_email) { this.okToEmail = ok_to_email; }

    @DynamoDBAttribute(attributeName = "phone")
    public String getPhone() { return phone; }
    public void setPhone(String phone) { this.phone = phone; }

    @DynamoDBAttribute(attributeName = "okToText")
    public boolean getOkToText() { return okToText; }
    public void setOkToText(boolean ok_to_text) { this.okToText = ok_to_text; }

    @DynamoDBAttribute(attributeName = "devices")
    public Set<String> getDevices() { return devices; }
    public void setDevices(Set<String> _terminals)
    {
        for (String element : _terminals) {
            this.setTerminal(element);
        }
    }

    public void setTerminal(String _terminal)
    {
        if(this.devices == null){
            this.devices = new HashSet<String>(); }

        if(!this.devices.contains(_terminal)){
            this.devices.add(_terminal);
        }
    }

    @DynamoDBAttribute(attributeName = "tagsActive")
    public Set<String> getTagsActive() { return tagsActive; }
    public void setTagsActive(Set<String> _tags_active)
    {
        for (String element : _tags_active) {
            this.setTagActive(element);
        }
    }

    public void setTagActive(String _tag)
    {
        if(this.tagsActive == null){
            this.tagsActive = new HashSet<String>(); }

        if(!this.tagsActive.contains(_tag)){
            this.tagsActive.add(_tag);
        }
    }

    @DynamoDBAttribute(attributeName = "tagsPending")
    public Set<String> getTagsPending() { return tagsPending; }
    public void setTagsPending(Set<String> _tags_pending)
    {
        for (String element : _tags_pending) {
            this.setTagPending(element);
        }
    }
    public void setTagPending(String _tag)
    {
        if(this.tagsPending == null){
            this.tagsPending = new HashSet<String>(); }

        if(!this.tagsPending.contains(_tag)){
            this.tagsPending.add(_tag);
        }
    }

    @DynamoDBAttribute(attributeName = "picture")
    public String getPicture() { return picture; }
    public void setPicture(String _picture) { this.picture = _picture; }

    @DynamoDBAttribute(attributeName = "dateAdded")
    public Date getDateAdded() { return dateAdded; }
    public void setDateAdded(Date dateAdded) { this.dateAdded = dateAdded; }


    @Override
    public String toString() {
        return "UserItem{" +
                "userId='" + getItemId() + '\'' + "\n" +
                ", userName='" + userName + '\'' + "\n" +
                ", email='" + email + '\'' + "\n" +
                ", okToEmail=" + okToEmail + "\n" +
                ", phone='" + phone + '\'' + "\n" +
                ", okToText=" + okToText + "\n" +
                ", devices=" + devices + "\n" +
                ", tagsActive=" + tagsActive + "\n" +
                ", tagsPending=" + tagsPending + "\n" +
                ", picture='" + picture + '\'' + "\n" +
                ", dateAdded=" + dateAdded + "\n" +
                '}';
    }
}


