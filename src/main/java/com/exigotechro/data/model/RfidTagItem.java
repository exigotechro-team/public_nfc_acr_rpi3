package com.exigotechro.data.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class RfidTagItem extends BaseItem
{
    private String tagStatus;
    private String label;
    private String description;
    private String color;
    private String shape;
    private String picture;
    private Set<String> devices;
    private String tagType;
    private String ownerId;
    private Date dateAdded;


    public String getTagId() { return getItemId();}
    public void setTagId(String _tagId) {this.setItemId(_tagId);}

    @DynamoDBAttribute(attributeName = "tagStatus")
    public String getTagStatus() { return tagStatus;}
    public void setTagStatus(String _tag_status) {this.tagStatus = _tag_status;}

    @DynamoDBAttribute(attributeName = "label")
    public String getLabel() { return label; }
    public void setLabel(String _label) { this.label = _label; }

    @DynamoDBAttribute(attributeName = "description")
    public String getDescription() { return description; }
    public void setDescription(String _description) { this.description = _description; }

    @DynamoDBAttribute(attributeName = "color")
    public String getColor() { return color; }
    public void setColor(String _color) { this.color = _color; }

    @DynamoDBAttribute(attributeName="shape")
    public String getShape() { return shape; }
    public void setShape(String shape) { this.shape = shape; }

    @DynamoDBAttribute(attributeName = "picture")
    public String getPicture() { return picture; }
    public void setPicture(String _picture) { this.picture = _picture; }

    @DynamoDBAttribute(attributeName = "devices")
    public Set<String> getDevices() { return devices; }
    public void setDevices(Set<String> _devices)
    {
        for (String element : _devices) {
            this.setDevice(element);
        }
    }

    public void setDevice(String _device)
    {
        if(this.devices == null){
            this.devices = new HashSet<String>(); }

        if(!this.devices.contains(_device)){
            this.devices.add(_device);
        }
    }

    @DynamoDBAttribute(attributeName = "tagType")
    public String getTagType() { return tagType; }
    public void setTagType(String tagType) { this.tagType = tagType; }


    @DynamoDBAttribute(attributeName="ownerId")
    public String getOwnerId() { return ownerId;}
    public void setOwnerId(String _owner_id) {this.ownerId = _owner_id;}

    @DynamoDBAttribute(attributeName = "dateAdded")
    public Date getDateAdded() { return dateAdded; }
    public void setDateAdded(Date dateAdded) { this.dateAdded = dateAdded; }


    @Override
    public String toString() {
        return "RfidTagItem{" +
                "tagId='" + getItemId() + '\'' + "\n" +
                ", tagStatus='" + tagStatus + '\'' + "\n" +
                ", label='" + label + '\'' + "\n" +
                ", description='" + description + '\'' + "\n" +
                ", color='" + color + '\'' + "\n" +
                ", shape='" + shape + '\'' + "\n" +
                ", picture='" + picture + '\'' + "\n" +
                ", devices=" + devices + "\n" +
                ", tagType='" + tagType + '\'' + "\n" +
                ", ownerId='" + ownerId + '\'' + "\n" +
                ", dateAdded=" + dateAdded + "\n" +
                '}';
    }


}


