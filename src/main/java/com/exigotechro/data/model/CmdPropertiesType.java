package com.exigotechro.data.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CmdPropertiesType
{
    @JsonProperty("data")
    private List<CmdPropertyType> data;

    public CmdPropertiesType() { }

    public CmdPropertiesType(List<CmdPropertyType> data) {
        this.data = data;
    }

    public List<CmdPropertyType> getData() {
        return data;
    }

    public void setData(List<CmdPropertyType> data) {
        this.data = data;
    }

    public void addProperty(CmdPropertyType cmdProperty)
    {
        if(this.data == null){
            this.data = new ArrayList<CmdPropertyType>(); }

        this.data.add(cmdProperty);
    }


    @Override
    public String toString()
    {
        String jsonStr = "";

//        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        ObjectWriter ow = new ObjectMapper().writer();
        try {
            jsonStr = ow.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return jsonStr;
    }




    public String toJSON()
    {
        String jsonStr = "";

//        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        ObjectWriter ow = new ObjectMapper().writer();
        try {
            jsonStr = ow.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return jsonStr;
    }

    public static CmdPropertiesType inflateFromJSON(String jsonStr)
    {
        CmdPropertiesType cps = new CmdPropertiesType();
        ObjectMapper mapper = new ObjectMapper();
        try {
            cps = mapper.readValue(jsonStr, CmdPropertiesType.class);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return cps;
    }
}
