package com.exigotechro.data.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

import java.text.SimpleDateFormat;

@DynamoDBTable(tableName="nfc_items")
public class BaseItem
{
    public static String TABLE_NAME     = "nfc_items";
    public static String TABLE_PK_ID    = "id";

    public static SimpleDateFormat DdbDateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");


    private String itemId;

    @DynamoDBHashKey(attributeName="id")
    public String getItemId() { return itemId;}
    public void setItemId(String _itemId) {this.itemId = _itemId;}

}
