package com.exigotechro.data.model;

import com.amazonaws.services.dynamodbv2.datamodeling.*;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.*;

public class CommandItem extends BaseItem
{
    /*
    ID: commands::commandId
    deviceId,
    terminals:[terminalId(s)...]
    tags:[tagId(s),...]
    commandStatus
    label
    resources:[
        { type: s3-file,        value: s3://brochure.pdf }
        { type: vcard,          value: file://sales.vcf }
        { type: text,           value: s3://msg }
        { type: html,           value: dynamodb://msg_id }
    ]
    principals:[
        { type: sns_arn,        value: <sns arn> }
        { type: email,          value: me@mail.com }
        { type: application,    value: /exigotechro/user/profile }
    ],
    **/

    private String deviceId;
    private Set<String> terminals;
    private Set<String> tags;
    private String commandStatus;
    private String label;
    private CmdPropertiesType resources;
    private CmdPropertiesType principals;
    private Date dateAdded;



    @DynamoDBAttribute(attributeName="deviceId")
    public String getDeviceId() { return deviceId; }
    public void setDeviceId(String deviceId) { this.deviceId = deviceId; }

    @DynamoDBAttribute(attributeName = "terminals")
    public Set<String> getTerminals() { return terminals; }
    public void setTerminals(Set<String> _terminals)
    {
        for (String element : _terminals) {
            this.setTerminal(element);
        }
    }

    public void setTerminal(String _terminal)
    {
        if(this.terminals == null){
            this.terminals = new HashSet<String>(); }

        if(!this.terminals.contains(_terminal)){
            this.terminals.add(_terminal);
        }
    }

    @DynamoDBAttribute(attributeName = "tags")
    public Set<String> getTags() { return tags; }
    public void setTags(Set<String> tags) {
        for (String element : tags) {
            this.setTag(element);
        }
    }

    public void setTag(String tag)
    {
        if(this.tags == null){
            this.tags = new HashSet<String>(); }

        if(!this.tags.contains(tag)){
            this.tags.add(tag);
        }
    }

    @DynamoDBAttribute(attributeName = "commandStatus")
    public String getCommandStatus() { return commandStatus; }
    public void setCommandStatus(String commandStatus) { this.commandStatus = commandStatus; }

    @DynamoDBAttribute(attributeName = "label")
    public String getLabel() { return label; }
    public void setLabel(String label) { this.label = label; }

    @DynamoDBAttribute(attributeName = "dateAdded")
    public Date getDateAdded() { return dateAdded; }
    public void setDateAdded(Date dateAdded) { this.dateAdded = dateAdded; }



    @DynamoDBTypeConverted(converter = CmdPropertiesTypeConverter.class)
    @DynamoDBAttribute(attributeName = "resources")
    public CmdPropertiesType getResources() {
        return resources;
    }

    @DynamoDBAttribute(attributeName = "resources")
    public void setResources(CmdPropertiesType _resources) {
        this.resources = _resources;
    }


    @DynamoDBTypeConverted(converter = CmdPropertiesTypeConverter.class)
    @DynamoDBAttribute(attributeName = "principals")
    public CmdPropertiesType getPrincipals() {
        return principals;
    }

    @DynamoDBAttribute(attributeName = "principals")
    public void setPrincipals(CmdPropertiesType _principals) {
        this.principals = _principals;
    }

} // end class