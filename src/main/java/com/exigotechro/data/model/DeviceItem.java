package com.exigotechro.data.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class DeviceItem extends BaseItem
{
    private String deviceStatus;
    private String deviceAccess;
    private Set<String> terminals;
    private Set<String> tagsActive;
    private Set<String> tagsPending;
    private String label;
    private String description;
    private String color;
    private String shape;
    private String picture;
    private String ownerId;
    private Date dateAdded;

    public String getDeviceId() { return getItemId();}
    public void setDeviceId(String _deviceId) {this.setItemId(_deviceId);}

    @DynamoDBAttribute(attributeName="ownerId")
    public String getOwnerId() { return ownerId;}
    public void setOwnerId(String _owner_id) {this.ownerId = _owner_id;}

    @DynamoDBAttribute(attributeName = "deviceStatus")
    public String getDeviceStatus() { return deviceStatus;}
    public void setDeviceStatus(String _device_status) {this.deviceStatus = _device_status;}

    @DynamoDBAttribute(attributeName = "deviceAccess")
    public String getDeviceAccess() { return deviceAccess;}
    public void setDeviceAccess(String _device_access) {this.deviceAccess = _device_access;}

    @DynamoDBAttribute(attributeName = "terminals")
    public Set<String> getTerminals() { return terminals; }
    public void setTerminals(Set<String> _terminals)
    {
        for (String element : _terminals) {
            this.setTerminal(element);
        }
    }

    public void setTerminal(String _terminal)
    {
        if(this.terminals == null){
            this.terminals = new HashSet<String>(); }

        if(!this.terminals.contains(_terminal)){
            this.terminals.add(_terminal);
        }
    }

    @DynamoDBAttribute(attributeName = "tagsActive")
    public Set<String> getTagsActive() { return tagsActive; }
    public void setTagsActive(Set<String> _tags_active)
    {
        for (String element : _tags_active) {
            this.setTagActive(element);
        }
    }

    public void setTagActive(String _tag)
    {
        if(this.tagsActive == null){
            this.tagsActive = new HashSet<String>(); }

        if(!this.tagsActive.contains(_tag)){
            this.tagsActive.add(_tag);
        }
    }

    @DynamoDBAttribute(attributeName = "tagsPending")
    public Set<String> getTagsPending() { return tagsPending; }
    public void setTagsPending(Set<String> _tags_pending)
    {
        for (String element : _tags_pending) {
            this.setTagPending(element);
        }
    }
    public void setTagPending(String _tag)
    {
        if(this.tagsPending == null){
            this.tagsPending = new HashSet<String>(); }

        if(!this.tagsPending.contains(_tag)){
            this.tagsPending.add(_tag);
        }
    }

    @DynamoDBAttribute(attributeName = "label")
    public String getLabel() { return label; }
    public void setLabel(String _label) { this.label = _label; }

    @DynamoDBAttribute(attributeName = "description")
    public String getDescription() { return description; }
    public void setDescription(String _description) { this.description = _description; }

    @DynamoDBAttribute(attributeName = "color")
    public String getColor() { return color; }
    public void setColor(String _color) { this.color = _color; }

    @DynamoDBAttribute(attributeName="shape")
    public String getShape() { return shape; }
    public void setShape(String shape) { this.shape = shape; }

    @DynamoDBAttribute(attributeName = "picture")
    public String getPicture() { return picture; }
    public void setPicture(String _picture) { this.picture = _picture; }

    @DynamoDBAttribute(attributeName = "dateAdded")
    public Date getDateAdded() { return dateAdded; }
    public void setDateAdded(Date dateAdded) { this.dateAdded = dateAdded; }


    @Override
    public String toString() {
        return "DeviceItem{" +
                "deviceId='" + getItemId() + '\'' + "\n" +
                ", deviceStatus='" + deviceStatus + '\'' + "\n" +
                ", deviceAccess='" + deviceAccess + '\'' + "\n" +
                ", terminals=" + terminals + "\n" +
                ", tagsActive=" + tagsActive + "\n" +
                ", tagsPending=" + tagsPending + "\n" +
                ", label='" + label + '\'' + "\n" +
                ", description='" + description + '\'' + "\n" +
                ", color='" + color + '\'' + "\n" +
                ", shape='" + shape + '\'' + "\n" +
                ", picture='" + picture + '\'' + "\n" +
                ", ownerId='" + ownerId + '\'' + "\n" +
                ", dateAdded=" + dateAdded + "\n" +
                '}';
    }



}


