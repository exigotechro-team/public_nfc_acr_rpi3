package com.exigotechro.data.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTypeConverter;

public class CmdPropertiesTypeConverter implements DynamoDBTypeConverter<String, CmdPropertiesType> {

    @Override
    public String convert(CmdPropertiesType object)
    {
        CmdPropertiesType cmdPropType = (CmdPropertiesType) object;
        return cmdPropType.toJSON();
    }

    @Override
    public CmdPropertiesType unconvert(String jsonStr)
    {
        return CmdPropertiesType.inflateFromJSON(jsonStr);
    }
}
