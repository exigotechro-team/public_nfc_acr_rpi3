package com.exigotechro.data.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;

public class CmdPropertyType
{
    @JsonProperty("propType")
    private String propType;

    @JsonProperty("propValue")
    private String propValue;

    public CmdPropertyType() { }

    public CmdPropertyType(String propType, String propValue) {
        this.propType = propType;
        this.propValue = propValue;
    }

    public String getPropType() {
        return propType;
    }

    public void setPropType(String propType) {
        this.propType = propType;
    }

    public String getPropValue() {
        return propValue;
    }

    public void setPropValue(String propValue) {
        this.propValue = propValue;
    }


    @Override
    public String toString()
    {
        String jsonStr = "";

        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        try {
            jsonStr = ow.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return jsonStr;
    }

    public String toJSON()
    {
        String jsonStr = "";

//        ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
        ObjectWriter ow = new ObjectMapper().writer();
        try {
            jsonStr = ow.writeValueAsString(this);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return jsonStr;
    }

}
