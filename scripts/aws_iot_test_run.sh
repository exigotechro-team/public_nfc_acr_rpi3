#!/usr/bin/env bash

java \
    -Dsun.security.smartcardio.library=/usr/lib/arm-linux-gnueabihf/libpcsclite.so \
    -cp "target/classes":"/usr/share/java/commons-daemon.jar":"/root/projects/rpi_acr122u_01/lib/third_party/lib/*":"/usr/share/java/aws_sdk/aws-java-sdk-1.11.126.jar":"/root/projects/rpi_acr122u_01/lib/acr122urw.jar":"/root/projects/rpi_acr122u_01/lib/nfctools.jar":"/root/projects/rpi_acr122u_01/lib/nfcjsvcd.jar" \
    com.exigotechro.mqtt.aws.PublishSubscribeSample
