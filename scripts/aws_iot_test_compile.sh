#!/usr/bin/env bash

javac \
    -cp "/usr/share/java/commons-daemon.jar":"/root/projects/rpi_acr122u_01/lib/third_party/lib/*":"/usr/share/java/aws_sdk/aws-java-sdk-1.11.126.jar":"/root/projects/rpi_acr122u_01/lib/acr122urw.jar":"/root/projects/rpi_acr122u_01/lib/nfctools.jar":"/root/projects/rpi_acr122u_01/lib/nfcjsvcd.jar" \
    -d target/classes \
    -sourcepath src/main/java \
    src/main/java/com/exigotechro/mqtt/aws/PublishSubscribeSample.java

